import React, { Component } from 'react';
import PlayerTemplate from './PlayerTemplate.js';
import './App.css';

class App extends Component {
  render() {
    return (
        <PlayerTemplate />
    );
  }
}

export default App;