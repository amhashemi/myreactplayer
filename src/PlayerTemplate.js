import React from "react";
import ReactPlayer from 'react-player'

/*
The goal is to create an audio player, similar to what you'd find at the bottom of the Spotify app.
All our media files are accessible via URLs, as you can see below in `this.tracks`. We're using a
library called react-player (https://www.npmjs.com/package/react-player) for the actual streaming
logic. Our MediaPlayer component encapsulates a ReactPlayer component.

The Player component should implement the following functionality (in order of priority):
1. It should have a play/pause button. Clicking on the button should play/pause the song
   accordingly.
2. It should display the track name, artist name, and artwork for the given track.
3. It should have next/previous buttons for navigating to the next/previous track in `this.tracks`.
4. Style it! The player should always appear at the bottom of the page, and should take up the full
   width of the screen.
5. A seeker for the song. It should grpahically show the amount of the song that has been played
   relative to the total length of the song. Look into progressInterval and onProgress in the
   ReactPlayer library.

Notes:
- Assume for now that we will always have a harcoded playlist in `this.tracks`.
- Feel free to add additional libraries if necessary.
- Prioritize a clean implementation.
- Launch localhost:3000 in the browser to view the result.
*/
class Player extends React.Component {
  constructor(props) {
    super(props);
    // This is the 'playlist' of tracks that we're playing/pausing, navigating through, etc.
    this.tracks = [
      {
        id: 1,
        trackName: "The Pretender",
        artistName: "Foo Fighters",
        artworkUrl: "https://images.sk-static.com/images/media/profile_images/artists/29315/huge_avatar",
        mediaUrl: "https://p.scdn.co/mp3-preview/6aba2f4e671ffe07fd60807ca5fef82d48146d4c?cid=1cef747d7bdf4c52ac981490515bda71",
        durationMilliseconds: 30000 // This track is 30 seconds long (30,000 milliseconds).
      },
      {
        id: 2,
        artistName: "Arctic Monkeys",
        trackName: "Do I Wanna Know?",
        artworkUrl: "https://cps-static.rovicorp.com/3/JPG_500/MI0003/626/MI0003626958.jpg?partner=allrovi.com",
        mediaUrl: "https://p.scdn.co/mp3-preview/9ec5fce4b39656754da750499597fcc1d2cc82e5?cid=1cef747d7bdf4c52ac981490515bda71",
        durationMilliseconds: 30000
      },
    ];
  }
  
  render() {
    return (
      <MediaPlayer tracks={this.tracks}/> 
    );
  }
}

/*
Library documentation: https://www.npmjs.com/package/react-player
*/
class MediaPlayer extends React.Component {
  constructor(props) {
    super(props);

    // set the current track index for the first time
    if (this.props.tracks && this.props.tracks.length)
        this.currentTrackIndex = 0;
    
    this.state = {
      url: this.currentUrl(),
      playing: false,
      played: 0
    }
  }

  render() {
    const { url, playing, played } = this.state

    return (
      <div className="player">
        {this.renderSlider(played)}
        {this.renderInfo()}
        {this.renderControls(playing)}
        
        <ReactPlayer
          ref={ m=> {this.player = m;}}
          onProgress={this.onProgress}
          onEnded={this.onEnded}
          playing={playing}
          controls={false}
          config={{ file: { forceAudio: true } }}
          url={url} /> 
      </div>
    )
  }

  renderSlider = played =>{
    return (
      <div className="slider">
        <input 
          type="range" 
          min={0} max={1}
          value={played} step="any" 
          onMouseDown={this.onSeekMouseDown}
          onChange={this.onSeekChange}
          onMouseUp={this.onSeekMouseUp} />    
      </div>  
    )
  }

  renderInfo = ()=>{
    return (
      <div className="info">
          <img src={this.currentTrack().artworkUrl} alt="Artwork"/>
          <div className="text">
            <div className="artist"> {this.currentTrack().artistName} </div>
            <div className="song"> {this.currentTrack().trackName} </div>
          </div>
        </div>
    );
  }

  renderControls = playing => {
    return (
      <div className="controls">
        <div>
          <i className={`fa fa-step-backward ${this.canPrevious() ? "hvr-grow" : "disabled"}`} 
            onClick={this.previous}>
          </i>
          <i className={`fa fa-2x hvr-grow large ${playing ? "fa-pause-circle" : "fa-play-circle"}`} 
            onClick={this.playPause}>
          </i>
          <i className={`fa fa-step-forward ${this.canNext() ? "hvr-grow" : "disabled"}`} 
            onClick={this.next}>
          </i>
        </div>
      </div>
    );
  }

  playPause = () => {
    this.setState({ playing: !this.state.playing })
  }

  next = () => {
    if (this.canNext()){
      this.currentTrackIndex++;
      this.setState({ url:this.currentUrl() });
    }
  }

  previous = () => {
    if (this.canPrevious()){
      this.currentTrackIndex--;
      this.setState({ url:this.currentUrl() });
    }
  }

  onProgress = state =>{
    if (!this.state.seeking)
      this.setState(state);
  }

  onSeekMouseDown = e => {
    this.setState({ seeking: true })
  }

  onSeekChange = e => {
    this.setState({ played: parseFloat(e.target.value) })
  }
  
  onSeekMouseUp = e => {
    this.setState({ seeking: false })
    this.player.seekTo(parseFloat(e.target.value))
  }

  onEnded = () => {
    if (this.canNext())
      this.next();
    else {
      this.setState({ playing: false });
      this.player.seekTo(0);
    }
  }

  canPrevious() {
    return this.currentTrackIndex > 0;
  }

  canNext() {
    return this.currentTrackIndex < this.props.tracks.length - 1;
  }

  currentTrack() {
    return this.currentTrackIndex >= 0 ? this.props.tracks[this.currentTrackIndex] : null;
  }

  currentUrl(){
    let track = this.currentTrack();
    return track? track.mediaUrl : null;
  }  
}

export default Player;